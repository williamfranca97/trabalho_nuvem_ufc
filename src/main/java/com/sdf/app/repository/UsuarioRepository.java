package com.sdf.app.repository;


import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sdf.app.dto.UsuarioDTO;
import com.sdf.app.model.Usuario;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long>
{
   @Query("SELECT new com.sdf.app.dto.UsuarioDTO(usr.codigo, usr.nome, usr.apelido, usr.email, usr.urlFoto) "
         + " FROM Usuario usr"
         + " WHERE lower(usr.apelido) like :apelido"
         + " OR null is :apelido"
         + " ORDER BY usr.apelido DESC")
   public List<UsuarioDTO> pesquisar(Pageable paginacao,@Param("apelido") String apelido) ;
}

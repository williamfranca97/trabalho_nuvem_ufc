package com.sdf.app;

import java.util.Locale;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.FixedLocaleResolver;

@SpringBootApplication
@EntityScan("com.sdf.*")
public class MainApplication extends SpringBootServletInitializer implements WebMvcConfigurer
{

   public static void main(String[] args)
   {
      SpringApplication.run(MainApplication.class, args);
   }
   
   @Bean
   public LocaleResolver localeResolver()
   {
      return new FixedLocaleResolver(new Locale("pt", "BR"));
   }

}

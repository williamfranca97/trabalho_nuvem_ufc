package com.sdf.app.dto;

import com.sdf.app.utils.BaseBean;

public class UsuarioDTO extends BaseBean
{
   private static final long serialVersionUID = 5935698395946325268L;

   private Long codigo;

   private String nome;

   private String apelido;

   private String senha;

   private String email;

   private String urlFoto;

   public UsuarioDTO()
   {
   }

   public UsuarioDTO(Long codigo, String nome, String apelido, String email, String urlFoto)
   {
      super();
      this.codigo = codigo;
      this.nome = nome;
      this.apelido = apelido;
      this.email = email;
      this.urlFoto = urlFoto;
   }

   public Long getCodigo()
   {
      return codigo;
   }

   public void setCodigo(Long codigo)
   {
      this.codigo = codigo;
   }

   public String getNome()
   {
      return nome;
   }

   public void setNome(String nome)
   {
      this.nome = nome;
   }

   public String getApelido()
   {
      return apelido;
   }

   public void setApelido(String apelido)
   {
      this.apelido = apelido;
   }

   public String getSenha()
   {
      return senha;
   }

   public void setSenha(String senha)
   {
      this.senha = senha;
   }

   public String getEmail()
   {
      return email;
   }

   public void setEmail(String email)
   {
      this.email = email;
   }

   public String getUrlFoto()
   {
      return urlFoto;
   }

   public void setUrlFoto(String urlFoto)
   {
      this.urlFoto = urlFoto;
   }
}

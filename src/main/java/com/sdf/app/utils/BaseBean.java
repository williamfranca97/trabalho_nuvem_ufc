package com.sdf.app.utils;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;

public abstract class BaseBean implements Serializable
{

   private static final long serialVersionUID = -7308397680473588764L;

   private Integer pagina;

   private Integer numeroMaximoRegistros;
   
   @Override
   public boolean equals(Object obj)
   {
      return EqualsBuilder.reflectionEquals(this, obj);
   }

   public Integer getPagina()
   {
      return pagina;
   }

   public void setPagina(Integer pagina)
   {
      this.pagina = pagina;
   }

   public Integer getNumeroMaximoRegistros()
   {
      return numeroMaximoRegistros;
   }

   public void setNumeroMaximoRegistros(Integer numeroMaximoRegistros)
   {
      this.numeroMaximoRegistros = numeroMaximoRegistros;
   }
}

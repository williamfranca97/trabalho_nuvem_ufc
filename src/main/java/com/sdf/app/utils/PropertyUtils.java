package com.sdf.app.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyUtils
{

    protected static Properties loadProperties(String resourceFileName) throws IOException
    {
        Properties configuration = new Properties();
        InputStream inputStream = PropertyUtils.class.getClassLoader().getResourceAsStream(resourceFileName);
        configuration.load(inputStream);
        inputStream.close();
        return configuration;
    }

}

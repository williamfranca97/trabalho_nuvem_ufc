package com.sdf.app.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.sdf.app.dto.UsuarioDTO;

@Service
public interface UsuarioService
{
   public List<UsuarioDTO> pesquisar(Integer pagina, String apelido);
   
   public UsuarioDTO inserir(UsuarioDTO dto) throws Exception;
   
   public UsuarioDTO alterar(UsuarioDTO dto) throws Exception;
   
}

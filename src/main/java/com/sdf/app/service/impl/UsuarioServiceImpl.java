package com.sdf.app.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.sdf.app.dto.UsuarioDTO;
import com.sdf.app.model.Usuario;
import com.sdf.app.repository.UsuarioRepository;
import com.sdf.app.service.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService
{

   @Autowired
   UsuarioRepository repository;
   
   @Override
   public UsuarioDTO inserir(UsuarioDTO dto) throws Exception
   {
      UsuarioDTO retorno = null;

      
      if (dto.getEmail().isEmpty())
      {
         throw new Exception("O campo email é obrigatório");
      }
      if(dto.getNome().isEmpty())
            {
         throw new Exception("O campo nome é obrigatório");
      }
      if(dto.getApelido().isEmpty())
      {
         throw new Exception("O campo apelido é obrigatório");
      }

      Usuario entity = new Usuario();
      
      entity.setEmail(dto.getEmail());
      entity.setNome(dto.getNome());
      entity.setSenha(dto.getSenha());;
      entity.setApelido(dto.getApelido());
      entity.setUrlFoto(dto.getUrlFoto());
      
      repository.save(entity);

      retorno = new UsuarioDTO(entity.getCodigo(), entity.getNome(), entity.getApelido(), entity.getEmail(), entity.getUrlFoto());

      return retorno;
   }

   @Override
   public UsuarioDTO alterar(UsuarioDTO dto) throws Exception
   {
      UsuarioDTO retorno = null;
      
      if (dto.getCodigo() == null)
      {
         throw new Exception("O campo código é obrigatório");
      }
      
      Optional<Usuario> optional = repository.findById(dto.getCodigo());
      if(optional.isPresent())
      {
         Usuario entity = optional.get();
         
         if(dto.getNome() != null)
         {
            entity.setNome(dto.getNome());
         }
         if(dto.getSenha() != null) 
         {
            entity.setSenha(dto.getSenha());;
         }
         if(dto.getUrlFoto() != null)
         {
            entity.setUrlFoto(dto.getUrlFoto());
         }
         repository.save(entity);
         
         retorno = new UsuarioDTO(entity.getCodigo(), entity.getNome(), entity.getApelido(), entity.getEmail(), entity.getUrlFoto());
      }
      return retorno;
   }

   @Override
   public List<UsuarioDTO> pesquisar(Integer pagina, String apelido)
   {
      Pageable paginacao = PageRequest.of(pagina, 5);
      return repository.pesquisar(paginacao,apelido);
   }
}

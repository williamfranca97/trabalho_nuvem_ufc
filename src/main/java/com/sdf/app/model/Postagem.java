package com.sdf.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

import com.sdf.app.utils.BaseBean;

@Entity
@Table(schema = "SC_CAD", name = "TBL_PST")
@Proxy(lazy = true)
public class Postagem extends BaseBean
{
   
   private static final long serialVersionUID = -5081124386375838970L;

   @Id
   @Column(name = "CD_PST")
   @GeneratedValue(strategy = GenerationType.AUTO, generator = "SQ_PST")
   @SequenceGenerator(name = "SQ_PST", sequenceName = "SC_CAD.SQ_PST", allocationSize = 1)
   private Long codigo;

   @Column(name = "URL_IMG_PST")
   private String urlImagem;

   @Column(name = "CD_USR")
   private Long codigoUsuario;
   
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "CD_USR", insertable = false, updatable = false)
   private Usuario usuario;

   @Column(name ="DSC_PST")
   private String descricao;
   
   public Long getCodigo()
   {
      return codigo;
   }

   public void setCodigo(Long codigo)
   {
      this.codigo = codigo;
   }

   public String getUrlImagem()
   {
      return urlImagem;
   }

   public void setUrlImagem(String urlImagem)
   {
      this.urlImagem = urlImagem;
   }

   public Long getCodigoUsuario()
   {
      return codigoUsuario;
   }

   public void setCodigoUsuario(Long codigoUsuario)
   {
      this.codigoUsuario = codigoUsuario;
   }

   public Usuario getUsuario()
   {
      return usuario;
   }

   public void setUsuario(Usuario usuario)
   {
      this.usuario = usuario;
   }

   public String getDescricao()
   {
      return descricao;
   }

   public void setDescricao(String descricao)
   {
      this.descricao = descricao;
   }
}

package com.sdf.app.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

import com.sdf.app.utils.BaseBean;

@Entity
@Table(schema = "SC_SGR", name = "TBL_USR")
@Proxy(lazy = true)
public class Usuario extends BaseBean
{

   private static final long serialVersionUID = -1915824851643592699L;

   @Id
   @Column(name = "CD_USR")
   @GeneratedValue(strategy = GenerationType.AUTO, generator = "SQ_USR")
   @SequenceGenerator(name = "SQ_USR", sequenceName = "SC_SGR.SQ_USR", allocationSize = 1)
   private Long codigo;

   @Column(name = "EML_USR")
   private String email;
   
   @Column(name = "NM_USR")
   private String nome;
   
   @Column(name = "SNH_USR")
   private String senha;
   
   @Column(name = "APL_USR")
   private String apelido;
   
   @Column(name = "URL_FTO_USR")
   private String urlFoto;
   
   @OneToMany(fetch = FetchType.LAZY, mappedBy = "usuario", cascade = CascadeType.ALL)
   private List<Postagem> listaPostagem;

   public Long getCodigo()
   {
      return codigo;
   }

   public void setCodigo(Long codigo)
   {
      this.codigo = codigo;
   }

   public String getEmail()
   {
      return email;
   }

   public void setEmail(String email)
   {
      this.email = email;
   }

   public String getNome()
   {
      return nome;
   }

   public void setNome(String nome)
   {
      this.nome = nome;
   }

   public List<Postagem> getListaPostagem()
   {
      return listaPostagem;
   }

   public void setListaPostagem(List<Postagem> listaPostagem)
   {
      this.listaPostagem = listaPostagem;
   }

   public String getSenha()
   {
      return senha;
   }

   public void setSenha(String senha)
   {
      this.senha = senha;
   }

   public String getApelido()
   {
      return apelido;
   }

   public void setApelido(String apelido)
   {
      this.apelido = apelido;
   }

   public String getUrlFoto()
   {
      return urlFoto;
   }

   public void setUrlFoto(String urlFoto)
   {
      this.urlFoto = urlFoto;
   }
}

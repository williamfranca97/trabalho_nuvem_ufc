package com.sdf.app.controller;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.sdf.app.dto.UsuarioDTO;
import com.sdf.app.service.UsuarioService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/usuario")
public class UsuarioController
{
   private static final Logger LOGGER = LoggerFactory.getLogger(UsuarioController.class);

   @Autowired
   private UsuarioService usuarioService;

   @ApiOperation(value = "Consulta de usuários")
   @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Consulta realizada com sucesso", response = UsuarioDTO.class, responseContainer = "List"),
      @ApiResponse(code = 204, message = "Nenhum registro encontrado"),
      @ApiResponse(code = 401, message = "Acesso negado"),
      @ApiResponse(code = 403, message = "Você não tem permissão"),
      @ApiResponse(code = 404, message = "Não encontrado"),
      @ApiResponse(code = 500, message = "Alguma falha detectada")
   })
   @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
   public ResponseEntity<Object> pesquisar(@RequestParam(name="apelido") String apelido,
         @RequestParam(name="pagina") Integer pagina)
   {
      List<UsuarioDTO> retorno = null;
      
      try
      {
         if(pagina == null)
         {
            pagina = 1;
         }
         if(apelido != null)
         {
            apelido = "%" + apelido.toLowerCase() + "%";
         }
         pagina--;
         retorno = usuarioService.pesquisar(pagina,apelido);
         
         if (retorno != null && !retorno.isEmpty())
         {
            Iterator<UsuarioDTO> iterator = retorno.iterator();
            while(iterator.hasNext())
            {
               UsuarioDTO elemento = iterator.next();
               LOGGER.info("Elemento: {}", elemento.toString());
            }
         }
         else 
         {
            LOGGER.info("Nenhuma noticia rapida encontrado");
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Nenhum registro encontrado");
         }
         
      }
      catch (Exception e)
      {
         LOGGER.error("Falha ao tentar consultar os usuarios.", e);
         return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
      }
      
      return ResponseEntity.status(HttpStatus.OK).body(retorno);
   }
   
   
   @ApiOperation(value = "Cadastro de usuário")
   @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Cadastro realizado com sucesso", response = UsuarioDTO.class),
      @ApiResponse(code = 204, message = "Nenhum registro encontrado"),
      @ApiResponse(code = 401, message = "Acesso negado"),
      @ApiResponse(code = 403, message = "Você não tem permissão"),
      @ApiResponse(code = 404, message = "Não encontrado"),
      @ApiResponse(code = 500, message = "Alguma falha detectada")
   })
   @PostMapping(path = "inserir", produces = MediaType.APPLICATION_JSON_VALUE)
   public ResponseEntity<?> inserir(@RequestParam("entity") String entityJson)
   {
      UsuarioDTO dto = new Gson().fromJson(entityJson, UsuarioDTO.class);
      UsuarioDTO retorno = null;
      
      try
      {
         LOGGER.info("DTO: {}", dto.toString());
         retorno = usuarioService.inserir(dto);
      }
      catch (Exception e)
      {
         LOGGER.error("Falha ao tentar inserir o usuário.", e);
         return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
      }
      
      return ResponseEntity.status(HttpStatus.OK).body(retorno);
   }
   
   @ApiOperation(value = "Alteração de usuário")
   @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Alteração realizada com sucesso", response = UsuarioDTO.class),
      @ApiResponse(code = 204, message = "Nenhum registro encontrado"),
      @ApiResponse(code = 401, message = "Acesso negado"),
      @ApiResponse(code = 403, message = "Você não tem permissão"),
      @ApiResponse(code = 404, message = "Não encontrado"),
      @ApiResponse(code = 500, message = "Alguma falha detectada")
   })
   @PutMapping(path = "alterar", produces = MediaType.APPLICATION_JSON_VALUE)
   public ResponseEntity<?> alterar(@RequestParam("entity") String entityJson)
   {
      UsuarioDTO dto = new Gson().fromJson(entityJson, UsuarioDTO.class);
      UsuarioDTO retorno = null;
      
      try
      {
         LOGGER.info("DTO: {}", dto.toString());
         
         retorno = usuarioService.alterar(dto);
      }
      catch (Exception e)
      {
         LOGGER.error("Falha ao tentar alterar o usuário.", e);
         return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
      }
      
      return ResponseEntity.status(HttpStatus.OK).body(retorno);
   }
   
   
}
